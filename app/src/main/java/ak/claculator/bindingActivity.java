package ak.claculator;

import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Locale;

public class bindingActivity extends AppCompatActivity {
    Button btnSave, persianButton, englishButton;
    EditText fullName, number, password;
    TextView result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_binding);
        binding();
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                result.setText(" Your password is " + password.getText() + " your number is " + number.getText()+" your fullname is " + fullName.getText());
//                buttonClick();
                Toast.makeText(bindingActivity.this, " Your password is " + password.getText() + " your number is " + number.getText().toString() + " your fullname is " + fullName.getText(), Toast.LENGTH_LONG).show();
            }
        });
    }


        private void binding () {
            password = findViewById(R.id.Password);
            number = findViewById(R.id.Number);
            fullName = findViewById(R.id.Full_Name);
            btnSave = findViewById(R.id.btn_Save);
            result = findViewById(R.id.Result);
        }

}