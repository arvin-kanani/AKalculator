package com.example.Framework;

public interface CalculatorFramework {
    double plus(double firstNumber, double secondNumber);
    double minus(double firstNumber, double secondNumber);
    double multipul(double firstNumber, double secondNumber);
    double divid(double firstNumber, double secondNumber);
}
