package CalculatorRunPackage;

import com.example.Framework.CalculatorFramework;
import com.example.Framework.CalculatorFrameworkRun;

import java.util.Scanner;

import Utils.UtilClass;

public class Run {
    public static void main(String[] args){
        Scanner scanner = new Scanner(System.in);

        UtilClass.ShowLine("Welcome,enter your first number please");
        double FirstNumber = scanner.nextDouble();

        UtilClass.ShowLine("Enter your second number");
        double SecondNumber = scanner.nextDouble();

        UtilClass.ShowLine("Enter your operator like(+, -, *, /)");
        String operator = scanner.next();

        CalculatorFrameworkRun frameworkRun = new CalculatorFrameworkRun();

        UtilClass.Show("Your number is: ");
        if(operator.equals("+")){
            frameworkRun.plus(FirstNumber, SecondNumber);

        }
        if(operator.equals("-")){
            frameworkRun.minus(FirstNumber, SecondNumber);

        }
        if(operator.equals("*")){
            frameworkRun.multipul(FirstNumber, SecondNumber);

        }
        if(operator.equals("/")){
            frameworkRun.divid(FirstNumber, SecondNumber);

        }

    }
}
